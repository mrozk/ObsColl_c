﻿using Microsoft.Win32;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.XWPF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ObsColl
{
    /// <summary>
    /// Логика взаимодействия для ExportWindow.xaml
    /// Powered by NPOI
    /// </summary>
    public partial class ExportWindow : Window
    {
        public ListCollectionView importCollection { get; set; }
        private List<String> importValuesList;
        private FileStream outFileStream;

        public ExportWindow()
        {
            InitializeComponent();
        }

        private void exportSaveDialogButton_Click(object sender, RoutedEventArgs e)
        {
            initdImportValueList();
            SaveFileDialog sd = new SaveFileDialog();
            sd.Filter = "*.xlsx|*.xlsx|*.docx|*.docx|*.csv|*.csv";
            if (sd.ShowDialog() == true)
            {
                try
                {
                    outFileStream = new FileStream(sd.FileName, FileMode.Create);

                    var extension = System.IO.Path.GetExtension(sd.FileName);
                    switch (extension.ToLower())
                    {
                        case ".xlsx":
                            exportToXls(outFileStream);
                            break;
                        case ".docx":
                            exportToDoc(outFileStream);
                            break;
                        case ".csv":
                            exportToCsv(outFileStream);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(extension);
                    }


                    outFileStream.Close();
                }
                catch (Exception exp)
                {
                    Console.WriteLine(exp.ToString());
                }
            }
        }

        private void closeSaveDialogButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void exportToXls(FileStream outFileStream)
        {
            XSSFWorkbook wb = new XSSFWorkbook();
            ISheet sheet = wb.CreateSheet("exported");
            int x = 0;
            foreach (var u in importCollection)
            {
                IRow row = sheet.CreateRow(x);
                int y = 0;
                foreach (String s in importValuesList)
                {
                    var inp = u.GetType().GetProperty(s).GetValue(u, null);
                    if (inp != null) {
                        row.CreateCell(y).SetCellValue(inp.ToString());
                    }
                    
                    //Console.WriteLine((string)u.GetType().GetProperty(s).GetValue(u, null));
                    y++;
                }
                x++;
            }
            wb.Write(outFileStream);
        }
        private void exportToDoc(FileStream outFileStream)
        {
            XWPFDocument doc = new XWPFDocument();
            XWPFParagraph p = doc.CreateParagraph();
            XWPFRun r0 = p.CreateRun();
            String text = "";
            int x = 0;
            foreach (var u in importCollection)
            {
                int y = 0;
                foreach (String s in importValuesList)
                {
                    if (y < importValuesList.Count - 1)
                    {
                        var v = u.GetType().GetProperty(s).GetValue(u, null);
                        if (v != null) {
                            text += v.ToString() + ",";
                        }
                    }
                    else
                    {
                        var v = u.GetType().GetProperty(s).GetValue(u, null);
                        if (v != null)
                        {
                            text += v.ToString() + "\n";
                        }
                        else {
                            text = text.Substring(0, text.Length - 1) + "\n";
                        }
                        
                        //text += Convert.ToString(u.GetType().GetProperty(s).GetValue(u, null)) + "\n";
                    }
                    //Console.WriteLine((string)u.GetType().GetProperty(s).GetValue(u, null));
                    y++;
                }
                x++;
            }
            r0.SetText(text);
            doc.Write(outFileStream);
        }
        private void exportToCsv(FileStream outFileStream)
        {
            String text = "";
            int x = 0;
            foreach (var u in importCollection)
            {
                int y = 0;
                foreach (String s in importValuesList)
                {
                    if (y < importValuesList.Count - 1)
                    {
                        var v = u.GetType().GetProperty(s).GetValue(u, null);
                        if (v != null)
                        {
                            text += v.ToString() + ",";
                        }
                    }
                    else
                    {
                        var v = u.GetType().GetProperty(s).GetValue(u, null);
                        if (v != null)
                        {
                            text += v.ToString() + "\n";
                        }
                        else
                        {
                            text = text.Substring(0, text.Length - 1) + "\n";
                        }
                    }
                    y++;
                }
                x++;
            }
            byte[] info = new UTF8Encoding(true).GetBytes(text);
            outFileStream.Write(info, 0, info.Length);
        }

        public void initdImportValueListBox()
        {
            if (importCollection.Count > 0)
            {
                foreach (PropertyInfo p in importCollection.GetItemAt(0).GetType().GetProperties())
                {
                    var cb = new CheckBox();
                    Console.WriteLine("---> " + p.Name);
                    cb.Content = p.Name;
                    exportColumnsList.Items.Add(cb);
                }
            }

        }
        public void initdImportValueList()
        {
            importValuesList = new List<String>();
            foreach (CheckBox x in exportColumnsList.Items)
            {
                if (x.IsChecked == true) { importValuesList.Add(x.Content.ToString()); }
            }

        }
    }
}
