﻿using BetterHttpClient;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ObsColl
{    
    /// <summary>
    /// Сlass represents remote proxy server object.
    /// Can be improve with implemenation of IPropertyChange interface.
    /// </summary>
    class ProxyNode 
    {
        private Boolean _status;
        public Boolean status
        {
            get { return _status; }
            set { _status = value; }
        }
        public String ip { get; set; }
        public String port { get; set; }
        public String type { get; set; }
        public String region { get; set; }
        public String anon { get; set; }
        public String ping { get; set; }
        public String name { get; set; }

        private Boolean _isLocked;
        public Boolean isLocked
        {
            get { return _isLocked; }
            set { _isLocked = value; }
        }
        private Boolean _isChecked;
        public Boolean isChecked
        {
            get { return _isChecked; }
            set { _isChecked = value; }
        }
        public String lastChek { get; set; }

        public ProxyNode(String ip, String port, String type, String region, String anon, String ping, String name, Boolean status)
        {
            this.ip = ip;
            this.port = port;
            this.type = type;
            this.region = region;
            this.anon = anon;
            this.ping = ping;
            this.name = name;
            this.status = status;

            this.isLocked = false;
            this.isChecked = false;
            //this.lastChek = new DateTime();
        }


    }
   
    /// <summary>
    /// Сlass represents object designed for cheking operability of remote proxy server object.
    /// Powered by https://github.com/Yozer/BetterHttpClient
    /// Can check HTTP,HTTPS,SOCKS
    /// </summary>
    class CheckSlave {
        public bool name { get; set; }
        public bool isBeasy { get; set; }
        delegate ProxyTypeEnum ProxyTypeEnumBuilder(String proxyType);
        public CheckSlave() {
            this.isBeasy= false;
        }
        public async void checkNode(ProxyNode proxyNode) {
            this.isBeasy = true;
            proxyNode.isLocked = true;

            ProxyTypeEnumBuilder buildProxyTypeEnum = (String proxyType) => {
                if (proxyType.Contains("SOCKS") || proxyType.Contains("socks")) return ProxyTypeEnum.Socks;
                return ProxyTypeEnum.Http;
            };

            Proxy p = new Proxy(proxyNode.ip, Int32.Parse(proxyNode.port), buildProxyTypeEnum(proxyNode.type));
            BetterHttpClient.HttpClient c = new BetterHttpClient.HttpClient(p);
            c.Timeout = TimeSpan.FromSeconds(10);
            c.NumberOfAttempts = 2;
            Uri uri = new Uri("http://mail.ru");

            String response = "";
            try {
                response = await c.DownloadStringTaskAsync(uri);
                response = response.Substring(0, 15);
                proxyNode.status = true;
                proxyNode.isChecked = true;
            } catch (Exception e) {
                response = e.ToString().Substring(0, 15);
            }
            Console.WriteLine(response);         
            proxyNode.lastChek = DateTime.Now.ToString("yyyy-MM-dd h:mm:ss");
            proxyNode.isLocked = false;
            
            this.isBeasy = false;

        }

    }

    /// <summary>
    /// Сlass represents object designed for cheking remote proxy server object item from ObservableCollection using pool of workers(CheckSlave).
    /// Work in distinct thread.
    /// </summary>
    class CheckMaster
    {

        protected Boolean isEnable;
        protected ObservableCollection<CheckSlave> slavesPool;
        protected ObservableCollection<ProxyNode> dataModel;

        int defaultWorkersCount = 10;
        public Int32 workersCount { get; set; }
        public ListCollectionView uncheckedView;
        public CheckMaster(ObservableCollection<ProxyNode> proxyNodesCollection) {
            this.dataModel = proxyNodesCollection;
            this.workersCount = defaultWorkersCount;
            this.isEnable = false;
            this.uncheckedView = new ListCollectionView(proxyNodesCollection);
            setFilter(this.uncheckedView);
            this.slavesPool = new ObservableCollection<CheckSlave>();
            for (int x = 0; x < workersCount; x++) {
                slavesPool.Add(new CheckSlave());
            }
            Thread myThread = new Thread(startCheckLoop);
            myThread.Start();
        }
        public CheckMaster(ObservableCollection<ProxyNode> proxyNodesCollection, int workersCount){
            this.dataModel = proxyNodesCollection;
            this.workersCount = workersCount;
            this.isEnable = false;
            this.uncheckedView = new ListCollectionView(proxyNodesCollection);
            setFilter(this.uncheckedView);
            this.slavesPool = new ObservableCollection<CheckSlave>();
            for (int x = 0; x < workersCount; x++)
            {
                slavesPool.Add(new CheckSlave());
            }
            Thread myThread = new Thread(startCheckLoop);
            myThread.Start();
        }
        public void startCheckLoop() {
            this.isEnable = true;
            while (this.isEnable == true) {             
                //select random proxyNode an process it in async/await
                if (uncheckedView.Count > 0) {
                    CheckSlave slave = getFreeSlave();
                    if (slave != null) {
                        Random rand = new Random();
                        Console.WriteLine(uncheckedView.Count.ToString() + "/" + rand.Next(uncheckedView.Count).ToString());
                        slave.checkNode((ProxyNode)uncheckedView.GetItemAt(rand.Next(uncheckedView.Count)));
                    }
                }
                Thread.Sleep(1000);
            }
        }
        private void setFilter(ListCollectionView view) {
            view.Filter = e => {
                ProxyNode p = e as ProxyNode;
                if (!p.isChecked && !p.isLocked)
                    return true;
                return false;
            };
        }
        private CheckSlave getFreeSlave() {
            foreach (CheckSlave slave in slavesPool) {
                if (!slave.isBeasy) return slave;
            }
            return null;
        }
        public void disable() { if (this.isEnable == true) this.isEnable = false; }
    }

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        ObservableCollection<ProxyNode> dataModel;
        ListCollectionView dataModelView;

        public ListCollectionView portsView;
        public ListCollectionView proxyTypesView;
        public ListCollectionView regionsView;
        public ListCollectionView anonimityView;

        CheckMaster c;

        public MainWindow() {
            InitializeComponent();

            dataModel = new ObservableCollection<ProxyNode>();
            findProxysNodes(dataModel);

            portsView = new ListCollectionView(dataModel);
            filterUniq(portsView,"port");
            portsComboBox.ItemsSource = portsView;

            proxyTypesView = new ListCollectionView(dataModel);
            filterUniq(proxyTypesView,"type");
            typesComboBox.ItemsSource = proxyTypesView;

            regionsView = new ListCollectionView(dataModel);
            filterUniq(regionsView, "region");
            regionsComboBox.ItemsSource = regionsView;

            anonimityView = new ListCollectionView(dataModel);
            filterUniq(anonimityView, "anon");
            anonimityComboBox.ItemsSource = anonimityView;
            
            dataModelView = new ListCollectionView(dataModel);
            dataGrid.ItemsSource = dataModelView;

            c = new CheckMaster(dataModel);
        }
        ~MainWindow() {
            c.disable();
        }


        /// <summary>
        /// Get list of unique values of object property from ListCollectionView ProxyNode objects
        /// </summary>
        private void filterUniq(ListCollectionView l, String propertyName) {
            List<String> uniqe = new List<String>();
            l.Filter = e => {
                ProxyNode p = e as ProxyNode;
                var value = p.GetType().GetProperty(propertyName).GetValue(p, null);
                if (uniqe.Contains(value.ToString())) {
                    return false;
                }
                uniqe.Add(value.ToString());
                return true;
            };
        }

        /// <summary>
        /// Methods for filtering data in datagrid
        /// </summary>
        void setFilter()
        {
            setFilter(dataModelView, filterString.Text);
        }
        void setFilter(ListCollectionView view, String filterString) {
            view.Filter = e =>
            {
                ProxyNode p = e as ProxyNode;

                bool allowByPort = true;
                bool allowByType = true;
                bool allowByRegions = true;
                bool allowByAnon = true;
                bool allowByString = true;

                if (portsComboBox.IsEnabled) {
                    if (((ProxyNode)portsComboBox.SelectedItem).port != p.port) allowByPort = false;
                }
                if (typesComboBox.IsEnabled)
                {
                    if (((ProxyNode)typesComboBox.SelectedItem).type != p.type) allowByType = false;
                }
                if (regionsComboBox.IsEnabled)
                {
                    if (((ProxyNode)regionsComboBox.SelectedItem).region != p.region) allowByRegions = false;
                }
                if (anonimityComboBox.IsEnabled)
                {
                    if (((ProxyNode)anonimityComboBox.SelectedItem).anon != p.anon) allowByAnon = false;
                }
                if (!String.IsNullOrEmpty(filterString)) {
                    String s = p.ip + p.port + p.type + p.name + p.region;
                    if (!s.Contains(filterString)) allowByString = false;
                }
                
                if (allowByString && allowByPort && allowByType && allowByRegions && allowByAnon)
                    return true;
                return false;
            };
        }

        /// <summary>
        ///async method for pasrsing one page from spys.ru
        ///Powed by HtmlAgilityPack, Jurassic
        /// </summary>
        async void findProxysNodes(ObservableCollection<ProxyNode> collection) {           
            var jsEngine = new Jurassic.ScriptEngine();
            String encodeKeyList = "";
            try
            {
                var httpClient = new System.Net.Http.HttpClient();
                var responceBody = await httpClient.GetStringAsync("http://spys.ru/proxies/");
                HtmlDocument html = new HtmlDocument();
                html.LoadHtml(responceBody);
                var token = html.DocumentNode.SelectNodes("//input[@name = 'xf0']")[0].GetAttributeValue("value", null);
                var postValues = new Dictionary<string, string> { { "xpp", "4" }, { "xf1", "0" }, { "xf0", token }, { "xf2", "0" }, { "xf4", "0" } };
                responceBody = await loadPage("http://spys.ru/proxies/", postValues);
                html = new HtmlDocument();
                html.LoadHtml(responceBody);
                HtmlNodeCollection jsNodes = html.DocumentNode.SelectNodes("//script[@type='text/javascript']");
                foreach (var node in jsNodes)
                {
                    String innerText = node.InnerText;
                    if (!innerText.Contains("document.write") && innerText.Contains('^')) encodeKeyList = innerText;
                }

                HtmlNodeCollection trNodes = html.DocumentNode.SelectNodes("//tr[@class='spy1x' or @class='spy1xx']");

                foreach (var node in trNodes)
                {
                    if (!String.IsNullOrEmpty(node.GetAttributeValue("onmouseover", null)))
                    {
                        String ip = node.FirstChild.ChildNodes[2].FirstChild.InnerText;
                        String port = "0";
                        try
                        {
                            port = jsEngine.Evaluate(encodeKeyList + node.FirstChild.ChildNodes[2].LastChild.InnerText.Replace("document.write(", "").Replace("))", ")")).ToString().Replace("<font class=spy2>:</font>", "");
                        }
                        catch (Exception e)
                        {
                            //Write error to log
                        }
                        String type = node.ChildNodes[1].ChildNodes[0].InnerText;
                        String anon = node.ChildNodes[2].ChildNodes[0].InnerText;
                        String ping = node.ChildNodes[3].ChildNodes[0].InnerText;
                        String region = node.ChildNodes[4].ChildNodes[0].InnerText;
                        String name = node.ChildNodes[5].ChildNodes[0].InnerText;
                        collection.Add(new ProxyNode(ip, port, type, region, anon, ping, name, false));
                    }
                }
            }
            catch (Exception ex) {
                Thread.Sleep(1000);
                findProxysNodes(collection);
            }           
        }
        async Task<string> loadPage(String url, Dictionary<string, string> postValues)
        {
            using (var client = new System.Net.Http.HttpClient())
            {
                String responseString = null;
                var content = new FormUrlEncodedContent(postValues);
                var response = await client.PostAsync(url, content);
                responseString = await response.Content.ReadAsStringAsync();
                return responseString;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            setFilter();
        }
        private void filterString_KeyUp(object sender, KeyEventArgs e)
        {
            if (isInteractiveStringFilter.IsChecked ?? false) {
                setFilter();
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            c.disable();
        }
        private void exportButton_Click(object sender, RoutedEventArgs e)
        {
            ExportWindow exportWindow = new ExportWindow();
            exportWindow.importCollection = dataModelView;
            exportWindow.initdImportValueListBox();
            exportWindow.ShowDialog();
        }
        private void dataGrid_AutoGeneratedColumns(object sender, EventArgs e)
        {
            dataGrid.Columns[7].Width = new DataGridLength(0.1, DataGridLengthUnitType.Star);
            dataGrid.Columns[10].Width = new DataGridLength(120);
        }
    }
}
